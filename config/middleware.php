<?php

use Selective\Config\Configuration;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Slim\Views\TwigMiddleware;


return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    $app->add(\App\Middleware\CorsMiddleware::class); // <--- here

    $app->add(TwigMiddleware::class); // <--- here

    // The RoutingMiddleware should be added after our CORS middleware so routing is performed first
    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);
};