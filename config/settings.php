<?php


error_reporting(0);
ini_set('display_errors', '0');

// Timezone
date_default_timezone_set('Europe/Berlin');

// Settings
$settings = [];

// Path settings
$settings['root'] = dirname(__DIR__);
$settings['temp'] = $settings['root'] . '/tmp';
$settings['public'] = $settings['root'] . '/public';
// $settings['upload'] = $settings['root'] . '/uploads'; --> in env


// Error Handling Middleware settings
$settings['error_handler_middleware'] = [

    // Should be set to false in production defined in env files
    // 'display_error_details' => true,

    // Parameter is passed to the default ErrorHandler
    // View in rendered output by enabling the "displayErrorDetails" setting.
    // For the console and unit tests we also disable it
    'log_errors' => true,

    // Display error details in error log
    'log_error_details' => true,
];

// $settings['db'] = [
//     'driver' => 'mysql',
//     'host' => '127.0.0.1',
//     'port' => '3306',
//     'charset' => 'utf8mb4',
//     'collation' => 'utf8mb_unicode_ci',
//     'prefix' => '',
//     'options' => [
//         // Turn off persistent connections
//         PDO::ATTR_PERSISTENT => false,
//         // Enable exceptions
//         PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
//         // Emulate prepared statements
//         PDO::ATTR_EMULATE_PREPARES => true,
//         // Set default fetch mode to array
//         PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
//         // Set character set
//         PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci',
//         //set rowcount mode
//         PDO::MYSQL_ATTR_FOUND_ROWS => true
//     ],
// ];


$settings['jwt'] = [
    //the issuer name
    'issuer' => 'webapp.ecs_website.be',

    // max lifetime in seconds
    'lifetime' => 14400,

    //the private key
    
    //the public key
    
];

$settings['twig'] = [
    // Template paths
    'paths' => [
        __DIR__ . '/../templates',
    ],
    // Twig environment options
    'options' => [
        // Should be set to true in production
        'cache_enabled' => false,
        'cache_path' => __DIR__ . '/../tmp/twig',
    ],
];

// Load environment configuration
if (file_exists(__DIR__ . '/../../env.php')) {
    require __DIR__ . '/../../env.php';
} else
if (file_exists(__DIR__ . '/env.php')) {
    require __DIR__ . '/env.php';
}

return $settings;



