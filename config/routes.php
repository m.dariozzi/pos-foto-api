<?php

use App\Action\PreflightAction;
use App\Middleware\JwtMiddleware;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    $app->get('/', \App\Action\HomeAction::class)->setName('home');;

    $app->get('/annotations', \App\Action\AnnotationAction::class . ':getAllFiles')->setName('annotations')->add(JwtMiddleware::class);;

    $app->get('/annotations/{id:[0-9]+}', \App\Action\AnnotationAction::class . ':getSingleFile')->setName('annotations')->add(JwtMiddleware::class);;

    $app->get('/annotations/getImageByName/{url}', \App\Action\AnnotationAction::class . ':getImageByPath')->setName('annotations')->add(JwtMiddleware::class);;;

    $app->get('/annotations/project/{project_id:[0-9]+}', \App\Action\AnnotationAction::class . ':getAnnotationByProject')->setName('annotations')->add(JwtMiddleware::class);;;

    $app->get('/annotations/author/{author_id:[0-9]+}', \App\Action\AnnotationAction::class . ':getAnnotationByAuthor')->setName('annotationss')->add(JwtMiddleware::class);;;

    $app->post('/annotations/save', \App\Action\AnnotationAction::class . ':saveAnnotations')->setName('annotations')->add(JwtMiddleware::class);;

    $app->post('/annotations/save_picture', \App\Action\AnnotationAction::class . ':savePicture')->setName('annotations')->add(JwtMiddleware::class);;


    /**projects */

    $app->get('/projects', \App\Action\ProjectAction::class . ':getAll')->setName('projects')->add(JwtMiddleware::class);;

    $app->get('/projects/{id:[0-9]+}', \App\Action\ProjectAction::class . ':getProject')->setName('projects')->add(JwtMiddleware::class);;

    $app->get('/projects/author/{id:[0-9]+}', \App\Action\ProjectAction::class . ':getProjectByAuthor')->setName('projects')->add(JwtMiddleware::class);;

    //@TODO -> bekijken hoe de rollen en de toegang van de projecten gaat werken.
    $app->get('/projects/role/{role}', \App\Action\ProjectAction::class . ':getProjectsByRole')->setName('projects')->add(JwtMiddleware::class);

    /**
     * authors
     */


    //een beetje overbodig, alleen de admin mag deze zien
    $app->get('/authors', \App\Action\AuthorsAction::class . ':getAll')->setName('authors')->add(JwtMiddleware::class);;

    $app->get('/authors/{id:[0-9]+}', \App\Action\AuthorsAction::class . ':getAuthorById')->setName('authors')->add(JwtMiddleware::class);;

    $app->get('/author/save', \App\Action\UserAction::class . ':createUser')->add(JwtMiddleware::class);;


    $app->group('/api', function (RouteCollectorProxy $group) { 

        $group->options('/tokens', PreflightAction::class);
        
        $group->post('/tokens', \App\Action\TokenCreateAction::class);
    
    });

    $app->group('/auth', function (RouteCollectorProxy $group) {
        $group->options('/login', PreflightAction::class);;
        
        $group->post('/login', \App\Action\UserAction::class . ':login');;

        $group->post('/register', \App\Action\UserAction::class . ':createUser')->add(JwtMiddleware::class);;
    });;
};