<?php

namespace App\Domain\User\Data;

final class UserLoginData {
    /** @var string */
    public $email;

    /** @var string */
    public $password;
}
