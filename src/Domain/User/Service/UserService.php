<?php

namespace App\Domain\User\Service;

use App\Domain\User\Data\UserLoginData;
use App\Domain\User\Repo\UserRepo;
use InvalidArgumentException;

final class UserService {

    private $repo;
    public function __construct(UserRepo $repo)
    {
        $this->repo = $repo;
    }   

    public function checkLogin($username, $password) {
        $user = new UserLoginData();
        $user->username = $username;
        $user->password = $password;

        $result = $this->login($user);

        if (empty($result)) {
            return false;
        }

        return true;
    }
    
    public function login(UserLoginData $data) {
        if (empty($data->username)) {
            throw new InvalidArgumentException('username is required');
        }
        if (empty($data->password)) {
            throw new InvalidArgumentException('password is required');
        }

        $user = $this->repo->login($data);

        if (empty($user)) {
            throw new InvalidArgumentException('wrong login credentials');
        } else {
            //if (password_verify($data->password, $user['password'])) {
			if ($data->password == $user['password']) {
                return $user;
            } else {
                throw new InvalidArgumentException('wrong login credentials');
            }
        }


    }

}