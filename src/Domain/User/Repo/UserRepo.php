<?php

namespace App\Domain\User\Repo;

use App\Domain\User\Data\UserLoginData;
use Selective\Config\Configuration;

class UserRepo {
    private $configuration;

    public function __construct(Configuration $conf)
    {
        $this->configuration = $conf;
    }

    public function login(UserLoginData $data) {
        
        //log de gebruiker in
        $url = $this->configuration->getArray('store')[0] . '/authors';
        $xml = simplexml_load_string(file_get_contents($url . '/authors_master.xml'));        

        $userObject = $xml->xpath('/root/author[username="'. $data->username .'"]');

        $xmltoJson = json_encode($userObject);
        $decodedXMLUserObject = json_decode($xmltoJson, TRUE);
        unset($decodedXMLUserObject[0]['@attributes']);
        return $decodedXMLUserObject[0];


    }
}   