<?php

namespace App\Domain\Annotation\Service;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

final class AnnotationService {

    private $repo;
    public function __construct()
    {
        
    }   


    function readAllAnnotations($url) {
		$rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($url));

        $files = array();

        foreach ($rii as $file) {

            if ($file->isDir()) {
                continue;
            }
            $files[] = $file->getPathname();
        }
        

        $xml = array();
        foreach ($files as $f) {
            $xml[] = simplexml_load_string(file_get_contents($f));
        }

        $xml = array_filter($xml);


        /**
         * Takes the pictures out of the picture elements, and converts it into an array by generating the 'picture+int' key
         */

        $final = array();
        foreach($xml as $el) {
            $temp_array = array();
            
            for ($int = 0; $int < $el->numberOfPictures; $int++) { 
                $pictureIndex = ${"picture$int"} = 'picture' . $int;
                
                array_push($temp_array, (string) $el->pictures->$pictureIndex);
                //remove picture from object
                unset($el->$pictureIndex);
            }
            //add picture array to object
            $el = (object) array_merge((array) $el, array( 'pictures' => $temp_array));

            array_push($final ,$el);
        }
		return $final;
    }





}