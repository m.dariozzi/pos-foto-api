<?php

namespace App\Domain\Project\Service;

final class ProjectService
{
    public function __construct() {

    }

    function readAllProjects($url) {
        $xml = simplexml_load_string(file_get_contents($url));
         
        $final = array();
            
        foreach($xml as $el) {
            array_push($final, $el);
        }

        $xml = array_filter($final);

        return $xml;
    }
}