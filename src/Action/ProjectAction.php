<?php

namespace App\Action;


use App\Domain\Project\Service\ProjectService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Selective\Config\Configuration;
use Slim\Routing\RouteContext;

final class ProjectAction
{
    private $configuration;
    private $service;

    public function __construct(Configuration $conf, ProjectService $service)
    {
        $this->configuration = $conf;
        $this->service = $service;
    }

    public function getAll(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        //retrive list 
        
        $url = $this->configuration->getArray('store')[0] . '/projects';
        $xml = simplexml_load_string(file_get_contents($url . '/projects_master.xml'));

        $final = array();
        
        foreach($xml as $el) {
            array_push($final, $el);
        }
        
        $response->getBody()->write(json_encode($final));
        return $response->withHeader('Content-Type', 'application/json');

    }


    public function getProject(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        //get single project
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();
        $id = $route->getArgument('id');

        $url = $this->configuration->getArray('store')[0] . '/projects';
        $xml = simplexml_load_string(file_get_contents($url . '/projects_master.xml'));


        $result = $xml->xpath('/root/project[@id=' . $id . ']');
    
        $response->getBody()->write(json_encode($result));
        return $response->withHeader('Content-Type', 'application/json');
    }   

    public function getProjectByAuthor(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();
        $id = $route->getArgument('id');

        $url = $this->configuration->getArray('store')[0] . '/projects/projects_master.xml';
        
        $xml = $this->service->readAllProjects($url);
        
        $result = array();
        $result = array_filter($xml, function($item) use ($id) {
			if($item->projectManagerId == $id) {
				return true;
			}
			return false;
		});

        $final = array();
		foreach($result as $el) {
			array_push($final, $el);
		}


        $response->getBody()->write(json_encode($final));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function save() {
        //save a new Project to the file.
    }

    public function getProjectsByRole(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();
        $role = $route->getArgument('role');

        $url = $this->configuration->getArray('store')[0] . '/projects';
        $xml = simplexml_load_string(file_get_contents($url . '/projects_master.xml'));


        // $response->getBody()->write(json_encode($result));
        return $response->withHeader('Content-Type', 'application/json');
    }
    
}
