<?php 

namespace App\Action;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface;

final class PreflightAction {
    
    public function __invoke(ServerRequestInterface $request, Response $response): Response
    {
        return $response;
    }
}