<?php

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Selective\Config\Configuration;
use Slim\Routing\RouteContext;

final class AuthorsAction
{
    private $configuration;

    public function __construct(Configuration $conf)
    {
        $this->configuration = $conf;
    }

    public function getAll(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface 
    {
        //retrive list 

        $url = $this->configuration->getArray('store')[0] . '/authors';
        $xml = simplexml_load_string(file_get_contents($url . '/authors_master.xml'));

        $final = array();
        
        foreach($xml as $el) {
            array_push($final, $el);
        }

        $response->getBody()->write(json_encode($final));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getAuthorById(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface 
    {
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();
        $id = $route->getArgument('id');

        $url = $this->configuration->getArray('store')[0] . '/authors';
        $xml = simplexml_load_string(file_get_contents($url . '/authors_master.xml'));

        $result = $xml->xpath('/root/author[@id=' . $id . ']');
       
    
        $response->getBody()->write(json_encode($result));
        return $response->withHeader('Content-Type', 'application/json');
    }
}
