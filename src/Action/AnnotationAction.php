<?php

namespace App\Action;

use App\Domain\Annotation\Service\AnnotationService;
use DOMDocument;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use Selective\Config\Configuration;
use SimpleXMLElement;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Routing\RouteContext;


final class AnnotationAction
{
	private $configuration;
	private $service;

	public function __construct(Configuration $conf, AnnotationService $service)
	{
		$this->configuration = $conf;
		$this->service = $service;
	}

	public function getAllfiles(
		ServerRequestInterface $request,
		ResponseInterface $response
	): ResponseInterface {
		//read contents of the folder declared in settings

		$url = $this->configuration->getArray('store')[0] . '/annotations';


		$final = $this->service->readAllAnnotations($url);
		//profi

		$response->getBody()->write(json_encode($final));
		return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
	}



	/**
	 * Get all annotations from server on Project id
	 *  */	
	public function getAnnotationByProject(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
		$routeContext = RouteContext::fromRequest($request);
		$route = $routeContext->getRoute();
		$project_id = $route->getArgument('project_id');
		
		$url = $this->configuration->getArray('store')[0] . '/annotations';

		$data = $this->service->readAllAnnotations($url);
		$arr = array_filter($data, function($item) use ($project_id){
				if ($item->projectId == $project_id) {
					return true;
				}
			return false;
		});

		$final = array();
		foreach($arr as $el) {
			array_push($final, $el);
		}

		$response->getBody()->write(json_encode($final));
		return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
	}

	public function getAnnotationByAuthor(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
		$routeContext = RouteContext::fromRequest($request);
		$route = $routeContext->getRoute();
		$author_id = $route->getArgument('author_id');

		$url = $this->configuration->getArray('store')[0] . '/annotations';

		$data = $this->service->readAllAnnotations($url);
		$arr = array_filter($data, function($item) use ($author_id) {
			if($item->authorId == $author_id) {
				return true;
			}
			return false;
		});

		$result = array();
		foreach($arr as $el) {
			array_push($final, $el);
		}


		$response->getBody()->write(json_encode($result));
		return $response->withHeader('Content-Type', 'application.json')->withStatus(200);

	}


	public function getSingleFile(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
		$routeContext = RouteContext::fromRequest($request);
		$route = $routeContext->getRoute();
		$id = $route->getArgument('id');

		$response->getBody()->write(json_encode(["test" => $id]));
		return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
	}


	/**
	 * DEPRECATED, USES BASE64 Encryption -- NOT USED ANYMORE
	 */
	public function saveAnnotations(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
		$data = (array) $request->getParsedBody();
		
		$xml = new SimpleXMLElement('<root/>');
		foreach ($data as $key => $value) {
			$xml->addChild($key, $value);
		}

		$xml->addChild('author_internal_code', $data['author']['POS_internal_code']);
		$xml->addChild('project_internal_code', $data['project']['POSInternalCode']);

		foreach ($data['pictures'] as $key => $value) {
			$xml->addChild('picture' . $key, $value);
		}
		
		$xml->addChild('numberOfPictures', sizeof($data['pictures']));


		$date = date('d-m-Y hh-mm-ss');

		$dom = new DOMDocument();
		$dom->loadXML($xml->asXML());    
		
		$savePath = 'ANN - ' . 'id= ' . $data['id'] . ', ' . $data['author']['POS_internal_code']
			 . ', ' . $data['project']['POSInternalCode'] . ', ' . $date . '.xml';

		$dom->save($url = $this->configuration->getArray('store')[0] . '/annotations/' . $savePath);

		//save de bijhorende foto's
		for ($i = 0; $i < sizeof($data['pictures']); $i++) {

			$savePath = 'Foto -id= ' . $data['id'] . ', ' + $i + 1 . 'of' . sizeof($data['pictures']) .', ' . $date . '.png'; 

			list($type, $imageData) = explode(';', $data['pictures'][$i]);	
			list(, $imageData) = explode(',', $data['pictures'][$i]);

			$imageData = base64_decode($imageData);

			file_put_contents($url = $this->configuration->getArray('store')[0] . '/annotations/' . $savePath, $imageData);	
		}

		$response->getBody()->write('ok');
		return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
	}



	public function savePicture(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
		$data = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        $photos = $files;

		$path = $this->configuration->getArray('store')[0] . '/annotations';
	

		$author = (array) json_decode($data['author']);
		$project = (array) json_decode($data['project']);

		$data['author'] = '';
		$data['project'] = '';

		$xml = new SimpleXMLElement('<root/>');
		foreach ($data as $key => $value) {
			$xml->addChild($key, str_replace(array('"', "'"), '', stripslashes($value)));
			
		}

		$xml->addChild('authorId', $author['authorId']);
		$xml->addChild('projectId', $project['projectId']);

		$xml->addChild('author_internal_code', $author['POS_internal_code']);
		$xml->addChild('project_internal_code', $project['POSInternalCode']);


		$xml->addChild('numberOfPictures', sizeof($files));
		$xml->addChild('pictures');

		if (!isset($_FILES)) {
            throw new InvalidArgumentException('no photo is uploaded');
        }		
		
		$filenames = array();

		foreach ($photos as $key => $photo) {
			if ($photo->getError() === UPLOAD_ERR_OK) {
				$filename = $this->moveUploadedFile($path, $photo);
				$xml->pictures->addChild($key, $filename);
				array_push($filenames, $filename);
			}	
		}

		$date = date('d-m-Y hh-mm-ss');

		$dom = new DOMDocument();
		$dom->loadXML($xml->asXML());    

		$savePath = 'ANN - ' . 'id= ' . $data['id'] . ', ' . $author['POS_internal_code']
			 . ', ' . $project['POSInternalCode'] . ', ' . $date . '.xml';

		$dom->save($url = $this->configuration->getArray('store')[0] . '/annotations/' . $savePath);
		

		$response->getBody()->write(json_encode($filenames));
		return $response->withHeader('Content-Type', 'application/json')->withStatus(200);	
	}


	public function getImageByPath(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $routeContext = RouteContext::fromRequest($request);
        $route = $routeContext->getRoute();
        $filename = $route->getArgument('url');

        $url = $this->configuration->getArray('store')[0] . '/annotations' . DIRECTORY_SEPARATOR . $filename;

        if (file_exists($url)) {
            $data = file_get_contents($url);
        } else {
            // $data = file_get_contents($this->configuration->getArray('upload')[0] . DIRECTORY_SEPARATOR . 'Knipsel.PNG');
        }

        $mime = getimagesize($url)['mime'];

        //SUPER MEGA BELANGRIJK DIE SHIT IS ESSENTIEEL!!!!ç!!!!!!!!!!!
        ob_clean();
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $response = $response->withHeader('Content-type', $mime);
        $response = $response->withHeader('Content-length', filesize($url));
        $response = $response->withBody((new StreamFactory())->createStream($data));
        return $response;
    }
	  
    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $directory directory to which the file is moved
     * @param UploadedFileInterface $uploaded file uploaded file to move
     * @return string filename of moved file
     */
    function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);
		
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        return $filename;
    }


	// Define a function that converts array to xml. 
	function arrayToXml($array, $rootElement = null, $xml = null) { 
		$_xml = $xml; 
		
		// If there is no Root Element then insert root 
		if ($_xml === null) { 
			$_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>'); 
		} 
		
		// Visit all key value pair 
		foreach ($array as $k => $v) { 
			
			// If there is nested array then 
			if (is_array($v)) {  
				
				// Call function for nested array 
				$this->arrayToXml($v, $k, $_xml->addChild($k)); 
				} 
				
			else { 
				
				// Simply add child element.  
				$_xml->addChild($k, $v); 
			} 
		} 
		
		return $_xml->asXML(); 
	} 


}