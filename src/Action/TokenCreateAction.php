<?php

namespace App\Action;

use App\Auth\JwtAuth;
use App\Domain\User\Service\UserService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class TokenCreateAction
{
    private $JwtAuth;
    private $service;

    public function __construct(JwtAuth $jwtAuth, UserService $service)
    {
        $this->JwtAuth = $jwtAuth;
        $this->service = $service;
    }
       
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $data = (array) $request->getParsedBody();

        $username = (string) ($data['username'] ?? '');
        $password = (string) ($data['password'] ?? '');

        //check login -> haal het author bestand op basis van email
        $isValidLogin = $this->service->checkLogin($username, $password);
        

        if (!$isValidLogin) {
            return $response->withHeader('Content-Type', 'application/json')
            ->withStatus(401, 'Unautorized');
        }

        $token = $this->JwtAuth->createJwt($username);
        $lifetime = $this->JwtAuth->getLifeTime();

        $result = [
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => $lifetime,
        ];


        $response = $response->withHeader('Content-Type', 'application/json');
        $response->getBody()->write((string) json_encode($result));

        return $response->withStatus(201);
    }
    
}