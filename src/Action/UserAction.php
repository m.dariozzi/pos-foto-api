<?php 

namespace App\Action;

use App\Domain\User\Data\UserLoginData;
use App\Domain\User\Service\UserService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Selective\Config\Configuration;

final class UserAction 
{
    private $service;

    public function __construct(Configuration $conf, UserService $service)
    {
        $this->service = $service;

    }

    public function login(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface 
    {
        $data = (array) $request->getParsedBody();

        $user = new UserLoginData();
        $user->username = $data['username'];
        $user->password = $data['password'];

        $user = $this->service->login($user);

        $response->getBody()->write((string) json_encode($user));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    }

}
