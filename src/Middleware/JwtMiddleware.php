<?php

namespace App\Middleware;

use App\Auth\JwtAuth;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Server\MiddlewareInterface;
use Slim\Psr7\Factory\ResponseFactory;

final class JwtMiddleware implements MiddlewareInterface
{
    /**
     * @var JwtAuth
     */
    private $jwtAuth;

    /**
     * @var ResponseFactoryInterface
     */
    private $ResponseFactory;

    public function __construct(JwtAuth $jwtAuth, ResponseFactoryInterface $responseFactory) 
    {
            $this->jwtAuth = $jwtAuth;
            $this->ResponseFactory = $responseFactory;
    }

    /**
     * Invoke middleware.
     *
     * @param ServerRequestInterface $request The request
     * @param RequestHandlerInterface $handler The handler
     *
     * @return ResponseInterface The response
     */
    public function process(\Psr\Http\Message\ServerRequestInterface $request, \Psr\Http\Server\RequestHandlerInterface $handler): \Psr\Http\Message\ResponseInterface
    {
        $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
        $token = $authorization[1] ?? '';

        if (!$token || !$this->jwtAuth->validateToken($token)) {
            return $this->ResponseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(401, 'Unauthorized');
        }

        $parsed_token = $this->jwtAuth->createParsedToken($token);
        $request = $request->withAttribute('token', $parsed_token);

        $request = $request->withAttribute('uid', $parsed_token->getClaim('uid'));
        
        return $handler->handle($request);
    }
}
